package com.example.swipeapp

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.swipeapp.Adapter.myAdapter
import com.example.swipeapp.Helper.MyButton
import com.example.swipeapp.Helper.MySwipeHeper
import com.example.swipeapp.Listener.MyButtonClickListener
import com.example.swipeapp.Model.Item
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    lateinit var adapter:myAdapter
    lateinit var layoutManager: LinearLayoutManager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        recycler_test.setHasFixedSize(true)
        layoutManager= LinearLayoutManager(this)
        recycler_test.layoutManager=layoutManager
        val swipe=object:MySwipeHeper(this,recycler_test,200){
            override fun instantiateMyButton(
                viewHolder: RecyclerView.ViewHolder,
                buffer: MutableList<MyButton>
            ) {
                buffer.add(
                    MyButton(this@MainActivity,
                "delete",
                30,
                0,
                Color.parseColor("#FF3C30"),
                    object:MyButtonClickListener{
                        override fun onClick(pos: Int) {
                            Toast.makeText(this@MainActivity,"DELETE ID"+pos,Toast.LENGTH_SHORT).show()
                        }

                    })
                )

                buffer.add(
                    MyButton(this@MainActivity,
                        "Update",
                        30,
                        R.drawable.ic_baseline_white_edit_24,
                        Color.parseColor("#FF9502"),
                        object:MyButtonClickListener{
                            override fun onClick(pos: Int) {
                                Toast.makeText(this@MainActivity,"UPDATE ID"+pos,Toast.LENGTH_SHORT).show()
                            }

                        })
                )
            }

        }
        generateItem()
    }

    private fun generateItem() {
        val itemList=ArrayList<Item>()
        var i=0
        while(i<50){
            itemList.add(Item("Pie 0"+ ++i ,"100,000$",
                "F:\\workProjectIranBirds\\swipeApp\\app\\src\\main\\res\\drawable"))
            i++
        }
        adapter= myAdapter(this,itemList)
        recycler_test.adapter=adapter
    }
}