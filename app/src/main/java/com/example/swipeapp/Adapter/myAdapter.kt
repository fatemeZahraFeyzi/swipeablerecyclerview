package com.example.swipeapp.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.swipeapp.Model.Item
import com.example.swipeapp.R

class myAdapter(internal  var context: Context,internal var itemList:MutableList<Item>) : RecyclerView.Adapter<myViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): myViewHolder {
        val itemview=LayoutInflater.from(context).inflate(R.layout.item_layout,parent,false)
        return myViewHolder(itemview)
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    override fun onBindViewHolder(holder: myViewHolder, position: Int) {
        Glide.with(context).load(itemList[position].image).into(holder.txt_cart_image)
        holder.txt_cart_price.text=itemList[position].price
        holder.txt_cart_name.text=itemList[position].name
    }
}