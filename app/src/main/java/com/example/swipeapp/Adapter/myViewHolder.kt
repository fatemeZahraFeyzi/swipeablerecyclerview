package com.example.swipeapp.Adapter

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_layout.view.*

class myViewHolder(itemview:View): RecyclerView.ViewHolder(itemview) {
    var txt_cart_name:TextView
    var txt_cart_price:TextView
    var txt_cart_image:ImageView
    init {
        txt_cart_image=itemview.card_image
        txt_cart_name=itemview.card_item_name
        txt_cart_price=itemview.card_item_price
    }
}